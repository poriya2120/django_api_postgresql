from rest_framework import routers
from employ.viewsets import EmployeeViewset

router=routers.DefaultRouter()
router.register('employee',EmployeeViewset)
